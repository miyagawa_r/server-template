CREATE TABLE IF NOT EXISTS account(
    id integer primary key autoincrement,
    name string,
    type string,
    access_key string,
    secret_key string,
    token string,
    token_expired_at timestamp,
    created_at timestamp NOT NULL DEFAULT (DATETIME('now', 'localtime')),
    updated_at timestamp NOT NULL DEFAULT (DATETIME('now', 'localtime'))
) IF account IS NOT EXIST

CREATE TRIGGER IF NOT EXISTS update_table after update on account
begin
update user set updated_at = DATETIME('now', 'localtime') WHERE id == NEW.id;
end;
